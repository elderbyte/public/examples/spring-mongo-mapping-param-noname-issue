# Spring Mongo Mapping Param Noname issue

## Description of the issue
If an entity contains a field of some type which is defined in another package, a mapping exception will be thrown during a read operation if said object doesn't provide an empty constructor (private, protected or public).

This exception happens since Spring Boot 3.2.0 and is not present in version 3.1.5, which suggest a possible regression.

## Structure
The project is split in two packages:
- `app` - Spring Boot Application with embedded MongoDB and spring data mongo dependency
- `other` - a library project which contains objects used by `app`

## Reproduce
Run test and analyse the `ApplicationTest` class in the test folder of the `app` package

## Description of the example
Project contains two packages:
- `com.mappingissue.app` - Spring Boot Application
- `com.mappingissue.other` - Package that contains some objects

Class `Cat` and `Collar` are defined in `com.mappingissue.app` package.
Class `CollarLib` is defined in `com.mappingissue.other` packege.
- app
  -- Cat
  -- Collar
- other
  -- CollarLib

Class `Cat` is used to persist and read data from the DB (Cat document).
Class `Cat` contains a field of type `Collar` and a field of type `CollarLib`.
Both `Collar` and `CollarLib` have the same fields, `String id` and `String company`

###  Cat.class `com.mappingissue.app`
```java
@Document
public class Cat {

    @Id
    private String id;
    private String name;
    private Collar collar;
    private CollarLib collarLib;
    
    ...
}   
```

### Collar.class `com.mappingissue.app`
```java
public class Collar {
    private String id;
    private String company;
    ...
}
```

### CollarLib.class `com.mappingissue.other`
```java
public class CollarLib {
    private String id;
    private String company;
    ...
}
```

When a Cat with a existing (non null) CollarLib is read, the MappingException is thrown.

Hence, for following tests, `success()` will pass but `fail_MappingException_ParameterDoesNotHaveAName` will fail with exception (Stack traced can be found at the end of the issue):

```java
    @Test
    public void success() {
        mongoOperations.save(
                new Cat(
                        "1",
                        "Sir Meow",
                        new Collar("John Doe", "123 Fake Street"),
                        null
                )
        );
        var cat = mongoOperations.find(
                Query.query(
                        Criteria.where("id").is("1")
                ),
                Cat.class
        ).get(0);

        Assertions.assertEquals("1", cat.getId());
    }

    @Test
    public void fail_MappingException_ParameterDoesNotHaveAName() {
        mongoOperations.save(
                new Cat(
                        "2",
                        "Lady Purr",
                        new Collar("1", "Collar Inc"),
                        new CollarLib("1", "Collar Inc")
                )
        );
        var cat = mongoOperations.find(
                Query.query(
                        Criteria.where("id").is("2")
                ),
                Cat.class
        ).get(0);

        Assertions.assertEquals("2", cat.getId());

    }
```

The issue is resolved if `CollarLib` in the `com.mappingissue.other`provides an empty constructor
