package com.elderbyte.mappingissue.other.cat;

public class CollarLib {

    private String id;
    private String company;

    public CollarLib(
            String id,
            String company
    ) {
        this.id = id;
        this.company = company;
    }

    public String getId() {
        return id;
    }

    public String getCompany() {
        return company;
    }
}
