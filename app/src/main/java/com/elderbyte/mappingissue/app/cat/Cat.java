package com.elderbyte.mappingissue.app.cat;

import com.elderbyte.mappingissue.other.cat.CollarLib;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.UUID;

@Document
public class Cat {

    @Id
    private String id;
    private String name;
    private Collar collar;
    private CollarLib collarLib;

    public Cat(
            String id,
            String name,
            Collar collar,
            CollarLib collarLib
    ) {
        this.id = id;
        this.name = name;
        this.collar = collar;
        this.collarLib = collarLib;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collar getCollar() {
        return collar;
    }

    public void setCollar(Collar collar) {
        this.collar = collar;
    }

    public CollarLib getCollarLib() {
        return collarLib;
    }

    public void setCollarLib(CollarLib collarLib) {
        this.collarLib = collarLib;
    }
}
