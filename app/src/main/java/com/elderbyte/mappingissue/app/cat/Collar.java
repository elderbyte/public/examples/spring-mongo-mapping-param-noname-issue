package com.elderbyte.mappingissue.app.cat;

public class Collar {

    private String id;
    private String company;


    public Collar(
            String id,
            String company
    ) {
        this.id = id;
        this.company = company;
    }

    public String getId() {
        return id;
    }

    public String getCompany() {
        return company;
    }
}
