package com.elderbyte.mappingissue.app;

import com.elderbyte.mappingissue.app.cat.Cat;
import com.elderbyte.mappingissue.app.cat.Collar;
import com.elderbyte.mappingissue.other.cat.CollarLib;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ExtendWith(SpringExtension.class)
@AutoConfigureDataMongo
public class ApplicationTest {

    @Autowired
    MongoOperations mongoOperations;

    @Test
    public void success() {
        mongoOperations.save(
                new Cat(
                        "1",
                        "Sir Meow",
                        new Collar("John Doe", "123 Fake Street"),
                        null
                )
        );
        var cat = mongoOperations.find(
                Query.query(
                        Criteria.where("id").is("1")
                ),
                Cat.class
        ).get(0);

        Assertions.assertEquals("1", cat.getId());
    }

    @Test
    public void fail_MappingException_ParameterDoesNotHaveAName() {
        mongoOperations.save(
                new Cat(
                        "2",
                        "Lady Purr",
                        new Collar("1", "Collar Inc"),
                        new CollarLib("1", "Collar Inc")
                )
        );
        var cat = mongoOperations.find(
                Query.query(
                        Criteria.where("id").is("2")
                ),
                Cat.class
        ).get(0);

        Assertions.assertEquals("2", cat.getId());

    }
}
